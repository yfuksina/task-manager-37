package ru.tsc.fuksina.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.service.ITokenService;

@Getter
@Setter
public class TokenService implements ITokenService {

    @NotNull
    private String token;

}
