package ru.tsc.fuksina.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonFasterXmlLoadRequest extends AbstractUserRequest {

    public DataJsonFasterXmlLoadRequest(@Nullable final String token) {
        super(token);
    }

}
