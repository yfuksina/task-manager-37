package ru.tsc.fuksina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.fuksina.tm.api.service.IProjectTaskService;
import ru.tsc.fuksina.tm.api.service.IServiceLocator;
import ru.tsc.fuksina.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.fuksina.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.fuksina.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.fuksina.tm.dto.response.TaskUnbindFromProjectResponse;
import ru.tsc.fuksina.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.fuksina.tm.api.endpoint.IProjectTaskEndpoint")
public final class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

}
