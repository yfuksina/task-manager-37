package ru.tsc.fuksina.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.IRepository;
import ru.tsc.fuksina.tm.comparator.CreatedComparator;
import ru.tsc.fuksina.tm.comparator.DateStartComparator;
import ru.tsc.fuksina.tm.comparator.StatusComparator;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    protected String getSort(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == DateStartComparator.INSTANCE) return "start_dt";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "name";
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final String sql = "SELECT * FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " ORDER BY " + getSort(comparator);
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Sort sort) {
        @NotNull final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Override
    public abstract M add(@NotNull final M model);

    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        List<M> result = new ArrayList<>();
        models.forEach(item -> result.add(add(item)));
        return result;
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final M model = fetch(resultSet);
        statement.close();
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@NotNull final M model) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, model.getId());
        statement.executeUpdate();
        statement.close();
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        collection.forEach(this::remove);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = "DELETE FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final String sql = "SELECT COUNT(1) FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        resultSet.next();
        final int size = resultSet.getInt(1);
        statement.close();
        return size;
    }

}
