package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.api.repository.IUserOwnedRepository;
import ru.tsc.fuksina.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
