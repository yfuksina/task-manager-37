package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
